# Prerequisites

Your workstation will need some software installed in order to use this development pattern.

- A working Python 2.7 installation, pip, and virtualenv
- [Vagrant 1.9]
- The [vagrant-hostsupdater] plugin.
    + Install with `vagrant plugin install vagrant-hostsupdater`
- The [vagrant-vbguest] plugin
    + Install with `vagrant plugin install vagrant-vbguest`
- [Virtualbox]
    + Install on a Mac using [Homebrew] and [Homebrew-Cask] by running `brew cask install virtualbox`

[Vagrant 1.9]: https://www.vagrantup.com/
[vagrant-hostsupdater]: https://github.com/cogitatio/vagrant-hostsupdater
[Virtualbox]: https://www.virtualbox.org/
[vagrant-vbguest]: https://github.com/dotless-de/vagrant-vbguest
[Homebrew]: https://brew.sh/
[Homebrew-Cask]: https://caskroom.github.io/

# Getting Started

From the root project directory, create a virtualenv and install our development requirements.

```
$ mkvirtualenv --no-site-packages django-webapp-boilerplate
(django-webapp-boilerplate)$ pip install -r requirements-dev.txt
```

This will install [Ansible], [Django], and their dependencies.

[Ansible]: https://ansible.com/
[Django]: https://www.djangoproject.com/

## Starting the development virtual machine

You can see the current status of project's virtual machine by running the `vagrant status` command:

```
(django-webapp-boilerplate)$ vagrant status
$ vagrant status
Current machine states:

dev                       not created (virtualbox)
```

We can start the dev virtual machine with the `vagrant up` command.

```
(django-webapp-boilerplate)$ vagrant up

Bringing machine 'dev' up with 'virtualbox' provider...
==> dev: Importing base box 'debian/jessie64'...
```

Vagrant will download an image of Debian 8 and provision it using Ansible.

To manually provision a box that is already up, run `vagrant provision`.

Ansible will install the software to support a Django deployment, update configuration files, and start services. Once complete, you can visit http://dev.local in your browser to see the Django welcome page.


## Developing with the virtual machine

You can edit the project on your workstation and Vagrant will sync changes to the virtual machine. Within the virtual machine, project files are stored in `/srv/webapp`.

To run the Django development server so that changes to project files reload the webapp server, you'll need to SSH into the virtual machine, turn off the `supervisor`-managed webapp, and run the dev server with `manage.py`.

```
(django-webapp-boilerplate)$ vagrant ssh

vagrant@dev:~$ sudo bash

root@dev:/home/vagrant# cd /srv/webapp

root@dev:/srv/webapp# supervisorctl stop webapp
webapp: stopped

(webapp) root@dev:/srv/webapp# python manage.py runserver
Performing system checks...

System check identified no issues (0 silenced).
August 01, 2017 - 16:56:12
Django version 1.11.3, using settings 'webapp.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

Now you can visit http://dev.local to test any changes to the project you make locally.

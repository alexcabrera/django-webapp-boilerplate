---
- name: install system requirements
  apt:
    name: "{{ item }}"
    state: latest
  with_items:
    - python-psycopg2

- name: install python project requirements
  pip:
    requirements: "{{ webapp_dir }}/requirements.txt"
    virtualenv: "{{ webapp_venv }}"

- name: copy environment variables
  template:
    src: "env.j2"
    dest: "{{ webapp_dir }}/.env"

- name: create postgresql database
  postgresql_db:
    name: "{{ webapp_database_name }}"
    state: present

- name: create postgresql database user
  postgresql_user:
    db: "{{ webapp_database_name }}"
    name: "{{ webapp_database_user }}"
    password: "{{ webapp_database_pass }}"
    role_attr_flags: "CREATEDB"
    state: present

- name: run migrations
  django_manage:
    command: migrate
    app_path: "{{ webapp_dir }}"
    virtualenv: "{{ webapp_venv }}"

- name: copy supervisor configuration
  template:
    src: "supervisor.j2"
    dest: "/etc/supervisor/conf.d/webapp.conf"

- name: copy nginx site configuration
  template:
    src: "nginx.site.j2"
    dest: "/etc/nginx/sites-available/webapp"

- name: enable nginx site configuration
  file:
    src: "/etc/nginx/sites-available/webapp"
    dest: "/etc/nginx/sites-enabled/webapp"
    state: link

- name: restart supervisor
  service:
    name: supervisor
    state: restarted

- name: restart nginx
  service:
    name: nginx
    state: restarted
